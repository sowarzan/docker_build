#!/bin/bash

declare -A BASE_URL

# These installer files are stored in a private CERNbox folder of Adrian Byszuk <adrian.byszuk@cern.ch>
# TODO: explore possibility of fetching them from DFS (needs CI runners with DFS access)
BASE_URL[2022.10]='https://cernbox.cern.ch/remote.php/dav/public-files/LZrJDlSROITHbxm/Riviera-PRO-2022.10.117-Linux64.run'

echo "Downloading Aldec Riviera Pro $1 installer from ${BASE_URL[$1]}"
curl ${BASE_URL[$1]} -o rivierapro_installer.run

