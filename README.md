# Dockerfiles for HDL EDA software based on https://gitlab.cern.ch/cce/docker_build/-/tree/master/ adapted by sowarzan

## Pulling an image
0. (only once) Login to container registry
```shell
docker login gitlab-registry.cern.ch
```
1. Pull a selected image:
```shell
docker pull gitlab-registry.cern.ch/sowarzan/docker_build/rivierapro:2022.10
```

## Additional image information

All images have defined environment variables for license servers and tool PATHs.  
These variables can be overriden as any other shell variable either during container startup:  
`docker run -e VARIABLE=value` or shell command:  
`export VARIABLE=value

---

### Aldec Riviera-PRO
Available under `gitlab-registry.cern.ch/sowarzan/docker_build/rivierapro:2022.10`<br />
Available versions: 2022.10 only.<br />
All images are based on CERN CentOS 8.<br />
All images contain full installation and precompiled Xilinx simulation libraries.<br />
Riviera is installed under `/opt/aldec/rivierapro/`.<br />
Precompiled Xilinx ISE libraries are available under path pointed by `RIVIERAPRO_ISE_LIBS` env variable.<br />
Precompiled Xilinx Vivado 2021.2 libraries are available under path pointed by `RIVIERAPRO_VIVADO_LIBS` env variable.<br />
Available libs: `unisim, simprim, xpm`, if you need additional Vivado/ISE libs just change `rivierapro/compile_vivado_libraries.tcl` file.<br />

Sourcing of Riviera `etc/setenv` and `etc/setgcc` is done through `/etc/profile.d/rivierapro.sh`.<br />
It's available for login shells, **but** the default entrypoint was also changed to `/bin/bash -l` so it should work also for non-interactive containers (i.e. Gitlab CI docker runners).<br />
If this doesn't work in CI you can always `source /opt/aldec/rivierapro/etc/setenv` as part of your `.gitlab-ci.yml` job, but please let me know that you have problems!<br />
All paths are set up right at the container startup.<br />

Additional software available: `X11 and mesa libs, git, nano, sudo, python3`.

---
